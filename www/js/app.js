/* ********************************
Init F7 Vue Plugin
outside pf Vue initialization
**********************************/

Vue.use(Framework7Vue);

// Handle device ready event
// Note: You may want to check out the vue-cordova package on npm for cordova specific handling with vue - https://www.npmjs.com/package/vue-cordova
document.addEventListener('deviceready', () => {
  console.log("DEVICE IS READY!");

}, false);

Parse.initialize('GmLm4QYwh9guOL2kacw311wEy0Dz2gMOps2gEGfv', '88qZlGdiuC43x3sBT8NmrXMGU0nxvKYEHEcQ48nB');
Parse.serverURL = 'https://parseapi.back4app.com/';

var currentUser = null;
var isLoggedIn = false;
var ExpenseData = {};

var clients = [{
    "Name": "Otto Clay",
    "Age": 25,
    "Country": 1,
    "Address": "Ap #897-1459 Quam Avenue",
    "Married": false
  },
  {
    "Name": "Connor Johnston",
    "Age": 45,
    "Country": 2,
    "Address": "Ap #370-4647 Dis Av.",
    "Married": true
  },
  {
    "Name": "Lacey Hess",
    "Age": 29,
    "Country": 3,
    "Address": "Ap #365-8835 Integer St.",
    "Married": false
  },
  {
    "Name": "Timothy Henson",
    "Age": 56,
    "Country": 1,
    "Address": "911-5143 Luctus Ave",
    "Married": true
  },
  {
    "Name": "Ramona Benton",
    "Age": 32,
    "Country": 3,
    "Address": "Ap #614-689 Vehicula Street",
    "Married": false
  }
];

var countries = [{
    Name: "",
    Id: 0
  },
  {
    Name: "United States",
    Id: 1
  },
  {
    Name: "Canada",
    Id: 2
  },
  {
    Name: "United Kingdom",
    Id: 3
  }
];


/*********************************************
 * About Page Component
 *********************************************/

Vue.component('page-about', {
  template: '#page-about',
  data() {
    return {
      currentUser: Parse.User.current().id
    }
  },
  methods: {
    check() {
      this.checked = !this.checked;
    }
  }
})


/*********************************************
 * Expense Form Page Component
 *********************************************/
Vue.component('scheduleF-expense-form', {
  template: '#scheduleF-expense-form',
  data() {
    return {
      currentUser: Parse.User.current().id
    }
  },
  methods: {
    submitExpense() {
      // var expenseData = myApp.formToJSON('#expense_form');

      var Farm_Expenses = Parse.Object.extend("Farm_Expenses");
      var farm_expenses = new Farm_Expenses();
      var loginUser = Parse.User.current();

      farm_expenses.set("expense_type", $('#expense_type').val());
      farm_expenses.set("expense_amount", parseInt($('#expense_amount').val(), 10));
      farm_expenses.set("expense_description", $('#expense_description').val());
      farm_expenses.set("expense_date", $('#expense_date').val());
      farm_expenses.set("created_by", loginUser);

      //farm_expenses.save(myApp.showIndicator(), {
      farm_expenses.save(null, {
        success: function (farm_expenses) {
          // Execute any logic that should take place after the object is saved.
          // myApp.hideIndicator();
          alert('New object created with objectId: ' + farm_expenses.id);
        },
        error: function (farm_expenses, error) {
          // myApp.hideIndicator();
          alert('Failed to create new object, with error code: ' + error.message);
        }
      });
    }
  }
})


/*********************************************
 * Expenses Page Component
 *********************************************/
Vue.component('page-expenses', {
  template: '#page-expenses',
  mounted: function () {
   //this.drawTable2();
  },
  created: function () {
    this.getUserExpenses();
  },

  data() {
    return {
      currentUser: Parse.User.current().id,
    }
  },
  methods: {
    getUserExpenses() {
      var Farm_Expenses = Parse.Object.extend("Farm_Expenses");
      var query = new Parse.Query(Farm_Expenses);

      var cu = Parse.User.current();
      query.equalTo("created_by", cu);
      query.find({
        success: function (results) {
          var r = JSON.stringify(results);
          console.log(JSON.parse(r));
          ExpenseData = r;
        },
        error: function (error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });
    },
    drawTable2() {
      var d = JSON.parse(ExpenseData);
      $("#jsGrid").jsGrid({
        width: "100%",
        height: "400px",

        sorting: true,
        paging: true,

        noDataContent: "No Record Found",
        loadIndication: true,
        loadIndicationDelay: 500,
        loadMessage: "Please, wait...",
        loadShading: true,

        data: d,

        fields: [{
            name: "expense_type",
            width: 150,
            validate: "required"
          },
          {
            name: "expense_amount",
            width: 50
          },
          {
            name: "expense_description",
            width: 200
          },
          {
            name: "expense_date",
            width: 200
          }
        ]
      });
    }
  }
})


/*********************************************
 * Main Vue Instance
 *********************************************/

// Init App
new Vue({
  el: '#app',
  // Init Framework7 by passing parameters here
  beforeCreate: function () {
    console.log('Nothing gets called before me!');
    // Parse.initialize('GmLm4QYwh9guOL2kacw311wEy0Dz2gMOps2gEGfv', '88qZlGdiuC43x3sBT8NmrXMGU0nxvKYEHEcQ48nB');
    // Parse.serverURL = 'https://parseapi.back4app.com/';
  },
  created: function () {
    this.checkUser();
  },
  data: {
    msg: "Welcome again ",
    user: Parse.User.current() ? Parse.User.current().get("username") : null,
    // username: null,
    // password: null,

    // expense_type: "",
    // expense_amount: "",
    // expense_description: "",
    // expense_date: ""
  },

  framework7: {
    root: '#app',
    /* Uncomment to enable Material theme: */
    //  vmaterial: true,
    routes: [{
        path: '/about/',
        component: 'page-about'
      },
      {
        path: '/scheduleF/',
        component: 'scheduleF-expense-form'
      },
      {
        path: '/form/',
        component: 'page-form'
      },
      {
        path: '/expenses/',
        component: 'page-expenses'
      },
      {
        path: '/dynamic-route/blog/:blogId/post/:postId/',
        component: 'page-dynamic-routing'
      }
    ],
  },
  methods: {
    checkUser: function () {
      isLoggedIn = Parse.User.current() ? true : false;
    },

    login: function () {
      // myApp.showIndicator();
      // var loginFormData = myApp.formToJSON('#login-form');
      var username = $('#username').val();
      var password = $('#password').val();
      Parse.User.logIn(username, password, {
        success: function (user) {
          // myApp.hideIndicator();
          location.reload();
        },
        error: function (user, error) {
          // The login failed. Check error to see why.
          alert("incorrect user name or password");
          //myApp.hideIndicator();
        }
      });
    },
    logout: function () {
      Parse.User.logOut();
      location.reload();
    }
  }
});